export class NewComments {
    author: string;
    rating: number;
    comment: string;
    date: string;
}