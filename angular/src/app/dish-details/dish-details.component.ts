import { Component, OnInit, Input, ViewChild, Inject } from '@angular/core';
import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { DishService } from '../services/dish.service';
import { Dish } from '../shared/dish';
import { switchMap } from 'rxjs/operators';
import { NewComments } from '../shared/comments';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-dish-details',
  templateUrl: './dish-details.component.html',
  styleUrls: ['./dish-details.component.scss']
})
export class DishDetailsComponent implements OnInit {

  disha: Dish;
  dishIds: string[];
  prev: string;
  next: string;

  commentsForm: FormGroup;
  
  newcomments: NewComments;
  @ViewChild('cform') commentsFormDirective;

  @Input() dish: Dish;

  autoTicks = false;
  disabled = false;
  invert = false;
  max = 5;
  min = 0;
  showTicks = true;
  step = 1;
  thumbLabel = true;
  value = 0;
  vertical = false;
  tickInterval = 1;

  formErrors = {
    'author': '',
    'comment': ''
  };

  validationMessages = {
    'author' : {
      'required': 'Name is required',
      'minlength': 'Name must be at least 2 characters long',
      'maxlength': 'Name can not be more than 25 characters long'
    },
    'comment' : {
      'required': 'Comment is required',
      'minlength': 'Comment must be at least 2 characters long',
      'maxlength': 'Comment can not be more than 25 characters long'
    }
  }

  errMess: string;
  dishCopy: Dish;


  constructor(private dishService: DishService, 
    private route: ActivatedRoute, 
    private location: Location, private fb: FormBuilder,
    @Inject('BaseURL') public BaseURL) {
      this.createForm();
    }

  ngOnInit() {
    this.dishService.getDishIds().subscribe((dishIds) => this.dishIds = dishIds);

    this.route.params.pipe(switchMap((params : Params) => this.dishService.getDish(params['id'])))
    .subscribe(dish => {
      
      this.dish = dish; 
      this.dishCopy = dish;
      console.log(this.dish);
      this.setPrevNext(dish.id);

    },
    errMess => this.errMess = <any>errMess);
  }

  setPrevNext( dishId: string){
    const index = this.dishIds.indexOf(dishId);
    this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
    this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];
  }

  goBack(): void {
    this.location.back();
  }

  createForm(){
    this.commentsForm = this.fb.group({
      'author': ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      'rating': 0,
      'comment': ['', [Validators.required, Validators.minLength(2), Validators.maxLength(25)]],
      'date': Date.now.toString()
    });

    this.commentsForm.valueChanges.subscribe(data => this.onValueChanged(data));
    this.onValueChanged(); // Resetting the feedback form
  }

  getSliderTickInterval(): number | 'auto' {
    if (this.showTicks) {
      return this.autoTicks ? 'auto' : this.tickInterval;
    }

    return 0;
  }

  onValueChanged(data?: any){
    if(!this.commentsForm){ return;}
    const form = this.commentsForm;

    for (const field in this.formErrors){
      if(this.formErrors.hasOwnProperty(field)){
        // Clear previous errors
        this.formErrors[field] = '';
        const control = form.get(field);
        if(control && control.dirty && !control.valid){
          const messages = this.validationMessages[field];
          for(const key in control.errors){
            if(control.errors.hasOwnProperty(key)){
              this.formErrors[field] += messages[key] + '';
            }
          }
        }
      }
    }
  }

  onSubmit(){
    // this.commentsForm.patchValue({
    //   date: new Date()
    // })
    this.newcomments = this.commentsForm.value;
    this.newcomments.date = new Date().toISOString();
    //this.dish.comments = [...this.dish.comments, this.newcomments]
    //this.dish.comments.push(this.newcomments);
    this.dishCopy.comments.push(this.newcomments);
    console.log(this.dishCopy);
    this.dishService.putDish(this.dishCopy).subscribe(dish => {
      console.log("dish", dish)
      this.dish = this.dishCopy; 
      this.dishCopy = dish;
    }, errMess => { 
      this.dish = null; 
      this.dishCopy = null; 
      this.errMess = <any>errMess
    });
    //console.log("this.dish", this.dish);
    //this.commentsFormDirective.resetForm();
    //this.commentsForm.reset();
    // this.commentsForm.patchValue({
    //   rating: 5
    // });
    //this.commentsFormDirective.resetForm();
  }
}
