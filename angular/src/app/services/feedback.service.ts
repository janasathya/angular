import { Injectable } from '@angular/core';
import { Feedback } from '../shared/feedback';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { baseURL } from '../shared/baseurl';
import { map, catchError, delay } from 'rxjs/operators';
import { ProcessHTTPMsgService } from './process-httpmsg.service';

@Injectable({
  providedIn: 'root'
})
export class FeedbackService {

  constructor(private http: HttpClient, private processHTTPMsgService: ProcessHTTPMsgService) { }

  putFeedback(feedback: Feedback): Observable<any>{
    const httpOptions = {
      headers : new HttpHeaders({
        'Content-Type': 'applications/json'
      })
    };
    console.log('feedback', feedback);
    return this.http.put<Feedback>(baseURL + 'feedbacks', feedback, httpOptions).pipe(catchError(this.processHTTPMsgService.handleError));
  }
}
